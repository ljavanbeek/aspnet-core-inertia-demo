﻿using InertiaAdapter;
using Microsoft.AspNetCore.Mvc;
using System.Threading;

namespace mvc.Controllers
{
    public class HomeController : Controller
    {

        public HomeController() {

        }

        public IActionResult Index()
        {
            return Inertia.Render("Home/Index", new {});
        }

        public IActionResult Other()
        {
            Thread.Sleep(2000);
            return Inertia.Render("Home2/Index", new {});
        }
    }
}
